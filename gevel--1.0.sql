-- complain if script is sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION gevel" to load this file. \quit

create or replace function gist_tree(text)
        returns text
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT;


create or replace function gist_tree(text,int4)
        returns text
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT;

create or replace function gist_stat(text)
        returns text
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT;

create or replace function gist_print(text)
        returns setof record
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT;

create or replace function gin_stat(text)
        returns setof record
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT;

create or replace function gin_stat(text, int)
        returns setof record
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT;

create or replace function gin_statpage(text)
        returns text
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT;

create or replace function gin_count_estimate(text, tsquery)
        returns bigint 
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT;

create or replace function spgist_stat(text)
        returns text 
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT;

create or replace function spgist_print(text)
        returns setof record
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT;
