# Gevel #

This is a PostgreSQL extension for inspecting generalised indexes such as GiST, GIN indexes. This version is a fork from git://sigaev.ru/gevel with changes to make the extension fit with the conventions used by other PGX extensions.

Documentation for Gevel can be found at the main site http://www.sai.msu.su/~megera/wiki/Gevel

## Compiling ##
Make sure a version of PostgreSQL is correctly compiled and installed, and that `pg_config` works as expected. You may also need to set library paths correctly via the `LD_LIBRARY_PATH` environment variable. Check the PostgreSQL manual for instructions. Once working, you should be able to go to the `gevel` parent directory 
and run

```
#!bash

make
make install
make installcheck
```