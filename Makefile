MODULES = gevel
EXTENSION = gevel
DATA = gevel--1.0.sql
DOCS = README.gevel
REGRESS = gevel

PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
